# `home.lan` rfc

This is the `home.lan` RFC project.

We collect specifications for APIs, file formats, wire protocols, and processes.

The change process is [C4](https://rfc.harbec.site/spec-2) with a few modifications:

* A specification SHOULD be created and modified by merge requests according to [RFC 2/C4](https://rfc.harbec.site/spec-2).
* Specification lifecycle SHOULD follow the lifecycle defined in [RFC 1/COSS](https://rfc.harbec.site/spec-1/COSS).
* Specification SHOULD use GPL v3 or a later version of the license.
* Specification SHOULD use [RFC 2119](https://tools.ietf.org/html/rfc2119) key words.
* Non-cosmetic changes are allowed only on Raw and Draft specifications.
* Each specification has an editor who publishes the RFC to https://rfc.harbec.site as needed.
