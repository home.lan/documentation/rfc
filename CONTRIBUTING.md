# Contributing to `home.lan` RFC

To be able to contribute, follow the C4 process at: https://rfc.harbec.site/spec-2/C4.

## Software Stack

In order to develop locally and test this RFC website, you will need install [GitBook CLI](https://github.com/GitbookIO/gitbook-cli) via the [NPM package manager](https://www.npmjs.com/).

```shell
# Install NPM package manager.
# This step depends heavely on what distro you are. i.e.: Linux (CentOS, SUSE, Ubuntu, etc.), macOS, Windows, etc.

# Move to project folder.
cd <project>

# Install GitBook CLI tools, globally, via NPM.
npm install -g gitbook-cli

# Fetch the specific version of GitBook which `home.lan` RFC support.
gitbook fetch 3.2.3

# Install the dependencies that `home.lan` RFC project requires (found within `book.js`).
gitbook install

# Will build the website (found within the current folder) and host it on localhost, for testing purpose.
gitbook serve

# Will build the website (found within the current folder).
gitbook build
```
